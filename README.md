<!--
SPDX-FileCopyrightText: 2023 ModExt Authors

SPDX-License-Identifier: CC-BY-4.0
-->

# ModExt-Example

Extracts a "star"-locality module using OWL API
